var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var should = chai.should()

var server = require('../server')

chai.use(chaiHttp) //configurar chai con el modulo http

describe('Test de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.com.mx')
    .get('/')
    .end((err,res) => {
      //console.log(res)
      res.should.have.status(200)
      done()
    })
  })
})


describe('Test de api de usuarios', () => {
  it('Raiz de api funciona', (done) => {
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err,res) => {
      //console.log(res)
      res.should.have.status(200)
      done()
    })
  })

  it('Raíz de la API funciona', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
        })
  })

  it('Raiz de la API devuelve 2 colecciones', (done) => {
      chai.request('http://localhost:3000')
          .get('/v3')
          .end((err, res) => {
            //console.log(res.body)
            res.should.have.status(200)
            res.body.should.be.a('array')
            res.body.length.should.be.eql(1000)
            done()
          })
    })

    


})
